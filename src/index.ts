// GOOGLE API: https://googleapis.dev/nodejs/googleapis/latest/

// CONFIGURE ENVIRONMENT
import { config } from 'dotenv';
config();

// START DBs
import { initMongoose, Oracle } from './Database';
initMongoose(process.env.MONGO_URI || '');
Oracle.createInstance();

// EXPRESS APP
import express from 'express';

// MIDDLEWARES
import cookieParser from 'cookie-parser';
import cors from 'cors';
import helmet from 'helmet';
import rateLimit from 'express-rate-limit';
import morgan from 'morgan';

// CONFIGURATION
const app = express();
app.use(cookieParser());
app.use(helmet());
app.use(morgan('tiny'));
app.use(express.json({
  limit: '5mb',
}));
app.use(cors({
  origin: [
    'localhost:3000',
    'lvh.me:3000',
    'oomar-den.cc',
  ],
}));
app.use(rateLimit({
  windowMs: 1 * 60 * 1000,    // 1 Minute
  max: 100,                   // 100 Requests
}));

// ROUTES
import Routes from './Routes';
app.use(Routes);


// START EXPRESS APP
const PORT = process.env.EXPRESS_PORT;
app.listen(PORT, () => console.log('Listening on Port', PORT));