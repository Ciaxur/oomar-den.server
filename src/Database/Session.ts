import { Schema, model } from 'mongoose';

export interface ISession {
  _id:            string,
  token:          string,
}

const SessionSchema = new Schema({
  token: String,
});
export const SessionModel = model('Session', SessionSchema);