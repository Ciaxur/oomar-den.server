import { Schema } from 'mongoose';
import { IAuthor } from './Author.schema';


const PaperType = [ 'Block', 'Article', 'White-Paper' ] as const;
export type IPaperType = typeof PaperType[number];
export interface IPaper {
  title:      string,
  body:       string,
  type:       IPaperType,
  author:     IAuthor,
  category:   string,
  tags:       string[],
  createdAt:  Date,
  updatedAt:  Date,
}

export const PaperSchema = new Schema<IPaper>({
  title:  { type: String, minLength: 4, maxLength: 128, required: true, trim: true },
  body:   { type: String, minLength: 1, required: true, trim: true },
  type:   { type: String, enum: PaperType, required: true },
  author: { type: 'ObjectId', ref: 'Author', required: true },
  category: { type: String, maxLength: 64, default: '', trim: true },
  tags:   { type: [ String ], default: [] },

  // INDEXED ENTRY
  _title:  { type: String, minLength: 4, maxLength: 128, required: true, trim: true, indexed: true, lowercase: true },
}, {
  timestamps: true,
});
