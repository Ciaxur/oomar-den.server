import Mongoose from 'mongoose';
import { IAuthor, AuthorSchema } from './Author.schema';
import { IPaper, PaperSchema } from './Paper.schema';
import { IDatabaseOptions } from '../index';

interface IAuthorDb extends IDatabaseOptions, IAuthor {
  _name: string,
}
interface IPaperDb extends IDatabaseOptions, IPaper {
  _title: string,
}

// Source of Truth, if import needs to be a General Database
export type { IPaper, IPaperType } from './Paper.schema';
export type { IAuthor } from './Author.schema';
export type { IAuthorDb, IPaperDb };


export default class BlogMap {
  private static instance: BlogMap | null = null;
  private conx: Mongoose.Connection;

/* eslint-disable @typescript-eslint/ban-types */
  public AuthorModel: Mongoose.Model<IAuthor, {}, {}>;
  public PaperModel: Mongoose.Model<IPaper, {}, {}>;
/* eslint-enable @typescript-eslint/ban-types */
  
  /**
   * Only this instance can instantite a BlogMap instance
   *  to respect the Singleton Pattern
   * @param uri BlogMap MongoDB URI
   */
  private constructor(uri: string) {
    // DEBUG: prints
    console.log('BlogMap Connecting to', uri);
    
    this.conx = Mongoose.createConnection(uri, {
      dbName: 'BlogMap',
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    // Create Models
    this.AuthorModel = this.conx.model('Author', AuthorSchema);
    this.PaperModel  = this.conx.model('Paper', PaperSchema);
  }

  /**
   * Creates a new BlogMap Instance
   * @param uri Optional Mongo URI, use ENV by default
   * @returns Returns new BlogMap instance
   */
  public static createInstance(uri = process.env.MONGO_URI): BlogMap {
    this.instance = new BlogMap(uri);
    return this.instance;
  }

  /**
   * Handles creating and returning a single instance
   * @returns BlogMap instance
   */
  public static getInstance(): BlogMap {
    if (this.instance)
      return this.instance;
    
    // No found Instance
    return this.createInstance();
  }
}