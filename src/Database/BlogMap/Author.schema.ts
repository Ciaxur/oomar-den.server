import { Schema } from 'mongoose';

export interface IAuthor {
  name:   string,
  _name:  string,    // Lowercase Indexed Name
}

export const AuthorSchema = new Schema<IAuthor>({
  name:   { type: String, minLength: 4, maxLength: 64, required: true, trim: true },
  
  // INDEXED ENTRY
  _name:  { type: String, minLength: 4, maxLength: 64, trim: true, indexed: true, lowercase: true, required: true },
});