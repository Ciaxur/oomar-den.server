import { Schema, model } from 'mongoose';

export interface IUser {
  _id:        string,
  userId:     string,
  oAuthId:    string | null,
  sessionId:  string | null,
}

const UserSchema = new Schema({
  userId:     { type: String, index: true, unique: true, trim: true },
  oAuthId:    { type: Schema.Types.ObjectId, ref: 'OAuth' },
  sessionId:  { type: Schema.Types.ObjectId, ref: 'Session' },
});
export const UserModel = model('User', UserSchema);