import Mongoose from 'mongoose';
import { IUrl, UrlSchema } from './Url.schema';
import { IDatabaseOptions } from '../index';

interface IUrlDb extends IDatabaseOptions, IUrl {}

// Source of Truth, if import needs to be a General Database
export type { IUrl, UrlSchema } from './Url.schema';
export type { IUrlDb };


export default class ShortUrl {
  private static instance: ShortUrl | null = null;
  private conx: Mongoose.Connection;
  
/* eslint-disable @typescript-eslint/ban-types */
  public UrlModel: Mongoose.Model<IUrl, {}, {}>;
/* eslint-enable @typescript-eslint/ban-types */
  
  /**
   * Setup Singleton Database
   * @param uri MongoDB URI
   */
  private constructor(uri: string) {
    console.log(`ShortUrl Connecting to ${uri}`);
    
    this.conx = Mongoose.createConnection(uri, {
      dbName: 'ShortUrl',
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    // Create Models
    this.initDbModels();
  }

  private initDbModels(): void {
    this.UrlModel = this.conx.model('Url', UrlSchema);
  }

  /**
   * Instance creation
   * @param uri Optional Mongo URI, use ENV by default
   * @returns Returns new ShortUrl instance
   */
  public static createInstance(uri = process.env.MONGO_URI): ShortUrl {
    this.instance = new ShortUrl(uri);
    return this.instance;
  }

  /**
   * Handles creating and returning a single instance
   * @returns ShortUrl Instance
   */
  public static getInstance(): ShortUrl {
    if (this.instance)
      return this.instance;
    
    // No found Instance
    return this.createInstance();
  }
}