import { Schema } from 'mongoose';

export interface IUrl {
  urlId:        string,     // Short URL Unique Identifier
  redirect?:    string,     // Redirect url string
  imageBuffer?: Buffer,     // Data buffer (image)
}

const urlRegex = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-/]))?/;
export const UrlSchema = new Schema<IUrl>({
  urlId: {
    type: String, minLength: 4, maxLength: 64, 
    required: true, unique: true, trim: true, 
    indexed: true, lowercase: true,
  },
  redirect: {
    type: String,
    validate: {
      validator: function(val: string) {
        return urlRegex.test(val);
      },
      message: () => 'Invalid URL!',
    },
    required: function(this: IUrl) {
      return !this.imageBuffer;
    },
  },
  imageBuffer: {
    type: Buffer,
    required: function(this: IUrl) {
      return !this.redirect;
    },
  },
});

