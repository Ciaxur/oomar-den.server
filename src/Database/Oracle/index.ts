import pg from 'pg';

export default class Oracle {
  private static instance: Oracle | null = null;
  public conx: pg.Client;

  /**
   * Only this instance can instantite an Oracle PSQL instance
   *  to respect the Singleton Pattern
   * @param uri URI to host Database
   * @param user User credentials
   * @param pass Password to DB
   * @param dbname Database Name
   */
  private constructor(uri: string, user: string, pass: string, dbname: string) {
    this.conx = new pg.Client(`postgres://${user}:${pass}@${uri}:5432/${dbname}`);
    this.conx.connect()
      .then(() => console.log('PQSL Connection Established'))
      .catch(err => console.log('PSQL Connection Error:', err));
  }

  /**
   * Creates a new Instance
   * @param uri URI to host Database
   * @param user User credentials
   * @param pass Password to DB
   * @param dbname Database Name
   * @returns Returns new BlogMap instance
   */
  public static createInstance(uri: string = process.env.PSQL_URL, user: string = process.env.PSQL_USER, pass: string = process.env.PSQL_PASS, dbname: string = process.env.PSQL_DB): Oracle {
    this.instance = new Oracle(uri, user, pass, dbname);
    return this.instance;
  }

  /**
   * Handles creating and returning a single instance
   * @returns Oracle instance
   */
  public static getInstance(): Oracle {
    if (this.instance)
      return this.instance;

    // No found Instance
    return this.createInstance();
  }

  /**
   * Queries total Guilds
   * @returns Number of Guilds found (0 if error or none)
   */
  public async getGuildCount(): Promise<number> {
    return this.conx.query('SELECT * FROM "Guild"')
      .then(res => res.rowCount)
      .catch(err => {
        console.log('Query Error:', err);
        return 0;
      });
  }
  
}