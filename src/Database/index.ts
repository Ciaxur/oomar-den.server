import Mongoose from 'mongoose';
import Oracle from './Oracle';

// Additional General DB Interfaces
export interface IDatabaseOptions {
  _id:        string,
  createdAt:  Date,
  updatedAt:  Date,
}

// Export all Models
import { OAuthModel, IOAuth } from './OAuth';
import { SessionModel, ISession } from './Session';
import { UserModel, IUser } from './User';

// Combined Models
interface IUserPopulated extends Omit<IUser, 'oAuthId' | 'sessionId' > {
  oAuthId:    IOAuth | null,
  sessionId:  ISession | null,
}

export {
  OAuthModel, IOAuth,
  SessionModel, ISession,
  UserModel, IUser,
  IUserPopulated,
  Oracle,
};

/**
 * Initializes mongoose connection
 * @param uri MongoDB URI
 * @returns Promise of connection
 */
export async function initMongoose(uri: string): Promise<void | typeof Mongoose> {
  return Mongoose.connect(uri, {
    dbName: 'oomar-den',
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
    .then(conx => {
      console.log('Mongoose Successfully Connected!');
      return conx;
    })
    .catch(err => console.log('Mongoose failed to connect,', err));
}
