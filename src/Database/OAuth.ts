import { Schema, model } from 'mongoose';

export interface IOAuth {
  _id:            string,
  email:          string,
  displayName:    string,
  givenName:      string,
}

const OAuthSchema = new Schema({
  email:          { type: String, lowercase: true, trim: true },
  displayName:    { type: String, trim: true },
  givenName:      { type: String, trim: true },
});
export const OAuthModel = model('OAuth', OAuthSchema);