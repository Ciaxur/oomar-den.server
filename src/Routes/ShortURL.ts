// ROUTER CONFIG
import { Router } from 'express';
import jwt from 'jsonwebtoken';
import { google } from 'googleapis';
import { ITokenPayload, sessionMap } from './Authentication';
const app = Router();

// DATABASE IMPORTS
import ShortUrl, {
  IUrl,
} from '../Database/ShortURL';

// ENV. VARS
const {
  TOKEN_SECRET,
} = process.env;

// GET: All Short Links, given query
app.get('/', async (_, res) => {
  const dbInstance = ShortUrl.getInstance();
  const urls = await dbInstance.UrlModel.find().limit(5);
  return res.json({
    urls,
  });
});

// GET: Specific url id
app.get('/:id', async (req, res) => {
  const urlDbInstance = ShortUrl.getInstance();
  const entry = await urlDbInstance.UrlModel.findOne({
    urlId: req.params.id,
  });

  // Entry found
  if (entry) {
    // Image Blob
    if (entry.imageBuffer) {
      return res.send(`
        <html>
          <body>
            <img src="data:image/png;base64,${entry.imageBuffer.toString('base64')}">
            </img>
          </body>
        </html>
      `);
    }

    // URL Redirect
    else {
      return res.redirect(entry.redirect);
    }
  }
  
  return res.json({
    id: req.params.id,
    message: `ShortUrl id '${req.params.id} not found'`,
  });
});

// POST: Create a new short url
// AUTH: OAuth Required
app.post('/', async (req, res) => {
  // AUTHENTICATE: Verify Session is found
  try {
    const { access_token } = req.cookies;
    const { uuid, expiresIn } = jwt.verify(access_token, TOKEN_SECRET) as ITokenPayload;
    const session = sessionMap[uuid];
    session.requests++;

    // Check for Expiration
    if (expiresIn < Date.now()) {
      delete sessionMap[uuid];
      return res
        .status(401)
        .clearCookie('access_token')
        .json({
          message: 'Access Token Expired 🔥',
        });
    }

    // Validate Client still kicking
    const people = await google.people({
      version: 'v1',
      auth: session.oauthClient,
    }).people.get({
      resourceName: 'people/me',
      personFields: 'emailAddresses,names',
    });

    // Early Error
    if (people.status !== 200)
      throw new Error('Google People Status Error');
  } catch (e) {
    return res.status(401).json({ message: 'User is not authorized 😠' });
  }
  
  // Expected Body
  const urlId: string = req.body.urlId;
  const redirect: string = req.body.redirect;
  const imageBuffer = req.body.imgBuffer;

  // Validate input prior to saving the data
  const urlDbInstance = ShortUrl.getInstance();
  const entry = new urlDbInstance.UrlModel({
    urlId,
    redirect,
    imageBuffer,
  } as IUrl);
  

  const r = entry.validateSync();
  if (r) {
    return res.json({
      error: r.message,
    });
  }

  // Add entry to DB
  return entry.save()
    .then((val: any) => res.json({ body: val }))
    .catch(err => res.json({
      error: err.message,
    }));
});


export default app;