import { Router } from 'express';
const app = Router();

// ROUTES IMPORT
import Authentication from './Authentication';
import Data from './Data';
import ShortUrl from './ShortURL';

// CONFIGURE ROUTES
app.use('/api/v1/auth', Authentication);
app.use('/api/v1/data', Data);
app.use('/api/v1/short', ShortUrl);


export default app;