/**
 * Data Router:
 *  - Handles Request for Authenticated Data
 */
import { Router } from 'express';
import jwt from 'jsonwebtoken';
import { google } from 'googleapis';
import { ITokenPayload, sessionMap } from './Authentication';
import BlogMap from '../Database/BlogMap';
import { Oracle } from '../Database';
import axios from 'axios';

// Configure Router
const app = Router();

// ENV. VARS
const {
  TOKEN_SECRET,
} = process.env;

interface IDataResponse {
  user: {
    displayName:    string,
  },
  urls: {
    rt_todo:        string,
    blogmap: {
      value:        string,     // Link Value
      totalPapers:  number,     // Total Number of Paper Entries
    },
    oracle: {
      value:        string,     // Link Value
      totalGuilds:  number,     // Total Number of connected Servers
    },
    topgg: {
      value:        string,     // Link Value of Bot
      status:       number,     // Status of Bot (HTTP Status Code)
    }
  },
}


/**
 * REQURED HEADERS:
 *  access_token - Used to Authenticate the User
 */
app.get('/', async (req, res) => {
  const { access_token } = req.cookies;

  try {
    // Verify Session is found
    const { uuid, expiresIn } = jwt.verify(access_token, TOKEN_SECRET) as ITokenPayload;
    const session = sessionMap[uuid];
    session.requests++;

    // Check for Expiration
    if (expiresIn < Date.now()) {
      delete sessionMap[uuid];
      return res
        .status(401)
        .clearCookie('access_token')
        .json({
          message: 'Access Token Expired 🔥',
        });
    }

    // Validate Client still kicking
    const people = await google.people({
      version: 'v1',
      auth: session.oauthClient,
    }).people.get({
      resourceName: 'people/me',
      personFields: 'emailAddresses,names',
    });

    // Early Error
    if (people.status !== 200)
      throw new Error('Google People Status Error');

    // Extract Data
    const { displayName } = people.data.names[0];
    
    // Get BlogMap Paper Entries
    const blogmap_instance = BlogMap.getInstance();
    let totalPapers = -1;    // Default
    try {
      totalPapers = await blogmap_instance.PaperModel.find().countDocuments();
    } catch(err) {
      console.log('BlogMap Query Error:', err);
    }

    // Get Oracle Server Connections
    const oracle_instance = Oracle.getInstance();
    const totalGuilds = await oracle_instance.getGuildCount();
    
    // Get Top.gg Bot Status
    let bot_statusCode = 500;
    const bot_uri = `https://top.gg/bot/${process.env.TOPGG_BOT_ID}`;
    try {
      const res = await axios.get(bot_uri);
      bot_statusCode = res.status;
    } catch (e) {
      bot_statusCode = e.response.status || 500;
    }
    
    // Return Data for User
    return res
      .status(200)
      .json({
        user: {
          displayName,
        },
        urls: {
          rt_todo: 'https://realtime-list-client.vercel.app',
          blogmap: {
            value: 'https://blog.oomar-den.cc',
            totalPapers,
          },
          oracle: {
            value: 'https://github.com/Ciaxur/DiscordStat.bot',
            totalGuilds,
          },
          topgg: {
            value: bot_uri,
            status: bot_statusCode,
          },
        },
      } as IDataResponse);
  } catch(e) {
    return res.status(401).json({ message: 'User is not authorized 😠' });
  }
});

export default app;
