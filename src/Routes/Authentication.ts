/**
 * Handles Supplying Authentication tokens to request.
 * Keeps track of the User's Session based on Google Auth Client
 *  - Storing Credentials
 *  - Verification
 */

// ROUTER CONFIG
import { Router } from 'express';
const app = Router();

// DATABASE
import { ISession, IUser, IUserPopulated, SessionModel, UserModel } from '../Database';

// GOOGLE & ID LIBRARIES
import { google } from 'googleapis';
import { OAuth2Client } from 'googleapis-common';
import { v4 as uuid4 } from 'uuid';
import jwt from 'jsonwebtoken';

// ENV. VARS
const {
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URL,
  TOKEN_SECRET,
} = process.env;

// CONFIGURE GOOGLE API

const oauth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URL,
);

const scopes = [
  'https://www.googleapis.com/auth/userinfo.email',
  'https://www.googleapis.com/auth/userinfo.profile',
];

// SESSION TRACKING & LIBRARIES
export interface IClientSession {
  uuid:         string,         // Unique Identifier
  oauthClient:  OAuth2Client,   // OAuth Client
  data: {                       // Data from People response
    id:           string,
    givenName:    string,
    displayName:  string,
    email:        string,
  },
  requests:     number,         // Number of Requests
}
interface ISessionTracking {
  [id: string]: IClientSession,
}
export const sessionMap: ISessionTracking = {};

// JWT Generate Token Object
export interface ITokenPayload {
  uuid:       string,   // UUID
  id:         string,   // People Google OAuth ID
  expiresIn:  number,   // Expiration
}

/**
 * Clears Session Object based on Token
 *  Handles errors
 * @param token JWT Token
 * @param payload Optional decrypted payload
 */
async function clearSession(token: string, payload?: ITokenPayload) {
  try {
    const { uuid } = payload 
      ? payload 
      : await jwt.verify(token, TOKEN_SECRET) as ITokenPayload;
    delete sessionMap[uuid];
  } catch (_) {
    console.log('clearSession: Could not clear Session. Session not found');
  }
}


// ROUTE HANDLERS
// GET: Requests OAuth URL
app.get('/', (_, res) => {
  // Generate OAuth2 URL
  const url = oauth2Client.generateAuthUrl({
    access_type: 'online',
    scope: scopes,
  });


  res.json({
    message: 'Google OAuth2.0 Requested. 🚀',
    url,
  });
});

// POST: Validates OAuth Code supplied
app.post('/', async (req, res) => {
  const { code } = req.query;
  const { access_token } = req.cookies;

  // Validate
  if (!code)
    return res.status(400).json({ error: 'No OAuth Code Provided in Query 😢' });
  
  // Clear out Token if any
  //  Since authorization was called again, just refresh
  //  the token
  if (access_token)
    await clearSession(access_token);
    
  // Validate OAuth Code
  try {
    // Create a Localized Auth Client for Request
    const authClient = new google.auth.OAuth2(
      process.env.CLIENT_ID,
      process.env.CLIENT_SECRET,
      process.env.REDIRECT_URL,
    );
    
    // Get Tokens & Register to Client
    const { tokens } = await authClient.getToken(code as string);
    authClient.credentials = tokens;

    // Get Info about User
    const people = await google.people({
      version: 'v1',
      auth: authClient,
    }).people.get({
      resourceName: 'people/me',
      personFields: 'emailAddresses,names',
    });

    // Get User Info & Keep track of session
    res.status(people.status);
    if (people.status === 200) {
      const { emailAddresses, names } = people.data;
      const { givenName, displayName, metadata } = names[0];
      const emailAddress = emailAddresses[0].value;

      // Allowed User
      const user: null | IUserPopulated = await UserModel
        .findOne({
          userId: metadata.source.id,
        } as Partial<IUser>)
        .populate('oAuthId')
        .populate('sessionId') as any;

      if (user && user.oAuthId && emailAddress.toLowerCase() === user.oAuthId.email) {
        // User Authenticated, keep track of them
        const sessionUUID = uuid4();
        sessionMap[sessionUUID] = {
          uuid: sessionUUID,
          oauthClient: authClient,
          data: {
            id: metadata.source.id,
            displayName,
            givenName,
            email: emailAddresses[0].value.toLowerCase(),
          },
          requests: 0,
        };

        // Generate Token
        const expire = Date.now() + (1000 * 60 * 60 * 12);    // 12 hrs
        const token = jwt.sign({
          uuid:       sessionUUID,
          id:         metadata.source.id,
          expiresIn:  expire,
        } as ITokenPayload, TOKEN_SECRET);

        // Add/Update token to database
        if (user.sessionId) {
          SessionModel.updateOne({
            _id: user.sessionId._id,
          } as Partial<ISession>, {
            token,
          } as Partial<ISession>);
        } else {
          SessionModel.create({
            token,
          } as Partial<ISession>)
            .then(entry => (
              UserModel.updateOne({
                _id: user._id,
              } as Partial<IUser>, {
                sessionId: entry._id,
              } as Partial<IUser>)
                .catch((err: any) => console.log('Authentication > User Session Update Error:', err))
            ));
        }


        return res
          .cookie('access_token', token, { expires: new Date(expire) })
          .status(200)
          .json({
            message: `User "${emailAddresses[0].value}" validated ✈️`,
          });
      }

      // Unauthorized User
      res.status(401);
    }

    // Not Authorized
    return res.json({ message: 'User not be authorized 😢' });
  } catch (e) {
    console.error('Authenticaton Error', e.response.data);
    return res.status(400).json({ message: 'User could not be authorized 😢' });
  }
});

// POST: Signs off from Session
app.post('/logoff', async (req, res) => {
  const { access_token } = req.cookies;

  try {
    if (access_token) {
      // Clear Cached Session
      const payload = await jwt.verify(access_token, TOKEN_SECRET) as ITokenPayload;
      await clearSession(access_token, payload);
      
      // Clear DB Session
      const user: IUser | null = await UserModel.findOne({
        userId: payload.id,
      } as Partial<IUser>) as any;
      
      if (user.sessionId) {
        await UserModel.updateOne({
          userId: payload.id,
        } as Partial<IUser>, {
          sessionId: null,
        } as Partial<IUser>);

        await SessionModel.deleteOne({
          _id: user.sessionId,
        } as Partial<ISession>);
      }

      return res
        .status(200)
        .json({
          message: 'User Logged off successfully',
        });
    }
  } catch(err) {
    console.log('Authentication > Logoff Error:', err);
    res
      .status(400)
      .json({
        message: 'Access Token could not be cleared',
      });
  }
});


export default app;